package dao

import (
	"crawler-service/models"

	"gorm.io/gorm"
)

type DriveLearningDAO interface {
	CreateCategory(category *models.Category) (*models.Category, error)
	CreateSetQuestion(setQuestion *models.SetQuestion) (*models.SetQuestion, error)
	CreateQuestion(question *models.Question) (*models.Question, error)
	CreateAnswer(answer *models.Answer) (*models.Answer, error)
}

type DriveLearningDAOImpl struct {
	gormHandler *gorm.DB
}

func NewDriveLearningDAO(gormHandler *gorm.DB) DriveLearningDAO {
	return &DriveLearningDAOImpl{gormHandler: gormHandler}
}

func (this *DriveLearningDAOImpl) CreateCategory(category *models.Category) (*models.Category, error) {
	err := this.gormHandler.Create(category).Error
	if err != nil {
		return nil, err
	}

	return category, nil
}

func (this *DriveLearningDAOImpl) CreateSetQuestion(setQuestion *models.SetQuestion) (*models.SetQuestion, error) {
	err := this.gormHandler.Create(setQuestion).Error
	if err != nil {
		return nil, err
	}

	return setQuestion, nil
}

func (this *DriveLearningDAOImpl) CreateQuestion(question *models.Question) (*models.Question, error) {
	err := this.gormHandler.Create(question).Error
	if err != nil {
		return nil, err
	}

	return question, nil
}

func (this *DriveLearningDAOImpl) CreateAnswer(answer *models.Answer) (*models.Answer, error) {
	err := this.gormHandler.Create(answer).Error
	if err != nil {
		return nil, err
	}

	return answer, nil
}
