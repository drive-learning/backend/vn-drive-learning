package dao

import (
	"crawler-service/models"

	orm "github.com/jinzhu/gorm"
)

type MangaDAO interface {
	FindById(id int) (*models.MangaInfo, error)
	Create(manga *models.MangaInfo) (*models.MangaInfo, error)
	Delete(id int) (*models.MangaInfo, error)
	Update(manga *models.MangaInfo) (*models.MangaInfo, error)

	FindAll() ([]*models.MangaInfo, error)
}

type MangaDAOImpl struct {
	ormHandler *orm.DB
}

func NewMangaDAO(ormHandler *orm.DB) MangaDAO {
	return &MangaDAOImpl{ormHandler: ormHandler}
}

func (this *MangaDAOImpl) FindById(id int) (*models.MangaInfo, error) {
	var mangaInfo = models.MangaInfo{}

	err := this.ormHandler.Where("id = ?", id).First(&mangaInfo).Error
	if err != nil {
		return nil, err
	}
	return &mangaInfo, nil
}

func (this *MangaDAOImpl) Create(manga *models.MangaInfo) (*models.MangaInfo, error) {
	return nil, nil
}
func (this *MangaDAOImpl) Delete(id int) (*models.MangaInfo, error) {
	return nil, nil
}
func (this *MangaDAOImpl) Update(manga *models.MangaInfo) (*models.MangaInfo, error) {
	return nil, nil
}

func (this *MangaDAOImpl) FindAll() ([]*models.MangaInfo, error) {
	return nil, nil
}
