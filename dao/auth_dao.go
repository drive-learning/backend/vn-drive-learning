package dao

import (
	"fmt"
	"crawler-service/models"

	orm "github.com/jinzhu/gorm"
)

type AuthDAO interface {
	FindByID(id int64) (*models.User, error)
	FindByPhoneNumber(phoneNumber string) (*models.User, error)
	FindByEmail(email string) (*models.User, error)
	FindAll() ([]*models.User, error)
	Create(user *models.User) (*models.User, error)
	Update(user *models.User) (*models.User, error)
	Delete(user *models.User) (*models.User, error)
	UpdateResetCode(userId int64, newCode string) (*models.User, error)
}

type AuthDAOImpl struct {
	ormHandler *orm.DB
}

func NewAuthDAO(ormHandler *orm.DB) AuthDAO {
	return &AuthDAOImpl{ormHandler: ormHandler}
}

func (this *AuthDAOImpl) FindByID(id int64) (*models.User, error) {
	var user = models.User{}
	err := this.ormHandler.Where("user_id = ?", id).First(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (this *AuthDAOImpl) FindAll() ([]*models.User, error) {
	return nil, nil
}

/// insert user to my sql database
func (this *AuthDAOImpl) Create(user *models.User) (*models.User, error) {

	fmt.Println(user)
	err := this.ormHandler.Create(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

/// update user info to database
func (this *AuthDAOImpl) Update(user *models.User) (*models.User, error) {

	/// check user info before update database
	err := user.BeforeSave()
	if err != nil {
		return nil, err
	}

	err = this.ormHandler.Save(user).Error

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (this *AuthDAOImpl) Delete(user *models.User) (*models.User, error) {
	return nil, nil
}

func (this *AuthDAOImpl) FindByPhoneNumber(phoneNumber string) (*models.User, error) {

	user := models.User{}
	err := this.ormHandler.Where("phone_number = ?", phoneNumber).First(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (this *AuthDAOImpl) FindByEmail(email string) (*models.User, error) {

	user := models.User{}
	err := this.ormHandler.Where("email = ?", email).First(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (this *AuthDAOImpl) UpdateResetCode(userId int64, newCode string) (*models.User, error) {
	user := models.User{}
	err := this.ormHandler.Model(&user).Where("user_id =?", userId).Update("reset_code", newCode).Error
	if err != nil {
		return nil, err
	}

	return &user, nil

}
