package models

type Episode struct {
	chapterNumberInString string
	mangaName             string
	updatedAtInString     string
}

func NewEpisode(
	mangaName string,
	chapterNumberInString string,
	updatedAtInString string) *Episode {
	return &Episode{
		mangaName:             mangaName,
		chapterNumberInString: chapterNumberInString,
		updatedAtInString:     updatedAtInString,
	}
}
