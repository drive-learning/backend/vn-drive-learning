package models

type MangaInfo struct {
	id                int
	name              string
	updatedAtInString string
	author            string
	statusInString    string
	typeInString      string
	description       string
	bannerUrlInString string
	descriptionTags   []string
}

func NewMangaInfo(
	id int,
	name string,
	updatedAtInString string,
	author string,
	statusInString string,
	typeInString string,
	description string,
	bannerUrlInString string, descriptionTags []string) *MangaInfo {

	return &MangaInfo{
		id:                id,
		name:              name,
		updatedAtInString: updatedAtInString,
		author:            author,
		statusInString:    statusInString,
		typeInString:      typeInString,
		description:       description,
		bannerUrlInString: bannerUrlInString,
		descriptionTags:   descriptionTags,
	}
}
