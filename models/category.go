package models

import "gorm.io/gorm"

type Category struct {
	gorm.Model
	Description    string
	Title          string
	UpdateStatus   string
	IsTestingType  bool
	IsLearningType bool
	ImageName      string
}
