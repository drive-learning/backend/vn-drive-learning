package models

import (
	"html"
	"strings"
	"time"

	validator "github.com/go-playground/validator/v10"
	orm "github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

/// used gorm for saved `User` infomation to database
type User struct {
	orm.Model
	UserID       int32      `gorm:"auto_increment,default:1" json:"user_id"`
	UserName     string     `gorm:"size:255; not null" json:"user_name" `
	Email        string     `gorm:"size:255" json:"email" validate:"omitempty,email"`
	PhoneNumber  string     `gorm:"size:100; unique" json:"phone_number" validate:"numeric,omitempty"`
	AvatarUrl    string     `gorm:"size:255" json:"avatar_url"`
	DateOfBirth  *time.Time `json:"date_of_birth"`
	Password     string     `gorm:"size:100;not null" json:"password"`
	ResetCode    string     `gorm:"size:10"`
	Active       bool       `json:"active"`
	Sex          string     `gorm:"size:100" json:"sex"`
	RefreshToken string     `gorm:"size:1000"`
}

/// hash password because password can not saved full description in database
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

/// open function for verify password of user
func VerifyPassword(hasedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hasedPassword), []byte(password))
}

func (this *User) BeforeSave() error {
	hasedPassword, err := Hash(this.Password)
	if err != nil {
		return err
	}
	this.Password = string(hasedPassword)
	return nil
}

func (this *User) PrePare() {
	this.ID = 0
	this.UserName = html.EscapeString(strings.TrimSpace(this.UserName))
	this.Email = html.EscapeString(strings.TrimSpace(this.Email))
	this.PhoneNumber = html.EscapeString(strings.TrimSpace(this.PhoneNumber))
}

const (
	UPDATE   = "update"
	LOGIN    = "login"
	REGISTER = "register"
)

func (this *User) Validate(validateType string) error {
	validate := validator.New()
	switch validateType {
	case UPDATE:

		err := validate.Struct(this)
		if err != nil {
			return err
		}

	case LOGIN:
		err := validate.Struct(this)
		if err != nil {
			return err
		}

	case REGISTER:
		err := validate.Struct(this)
		if err != nil {
			return err
		}
	}

	return nil
}
