package models

import "gorm.io/gorm"

type SetQuestion struct {
	gorm.Model
	CategoryId       uint
	Title            string
	LearningProgress float64
}
