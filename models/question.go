package models

import (
	"gorm.io/gorm"
)

type Question struct {
	gorm.Model
	Description     string
	ImageName       string
	SetQuestionId   uint
	IsHasImage      bool
	Title           string
	AnswerExplained string
}

// func NewQuesion(
// 	description string,
// 	id string, imageName string,
// 	setQuestionId string,
// 	isHasImage bool) *Question {
// 	return &Question{Description: description,
// 		QuestionId:    id,
// 		ImageName:     imageName,
// 		SetQuestionId: setQuestionId,
// 		IsHasImage:    isHasImage}
// }
