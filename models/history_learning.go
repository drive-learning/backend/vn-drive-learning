package models

import "gorm.io/gorm"

type HistoryLearning struct {
	gorm.Model
	SetQuestionId      string
	TotalCorrectAnswer int
	TotalQuestion      int
}
