package models

type EpisodePage struct {
	pageNumber  int
	srcImageUrl string
}

func NewEpisodePage(
	pageNumber int,
	srcImageUrl string) *EpisodePage {

	return &EpisodePage{
		srcImageUrl: srcImageUrl,
		pageNumber:  pageNumber,
	}
}