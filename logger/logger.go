package logger

import logging "github.com/op/go-logging"

var WorkoutLogger = logging.MustGetLogger("Workout-App-Sample")
