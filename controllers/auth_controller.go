package controllers

import (
	"fmt"
	"net/http"
	"crawler-service/auth"
	"crawler-service/common"
	"crawler-service/dto"
	"crawler-service/logger"
	"crawler-service/models"
	"crawler-service/reqModels"
	"crawler-service/service"

	"github.com/gin-gonic/gin"
)

type AuthController struct {
	BaseController
	AuthService  service.AuthService
	MailService  service.MailService
	CacheService service.CacheService
}

var Auth AuthController

func (this *AuthController) PrePare() {
	this.AuthService = service.Provider.GetAuthService()
	this.MailService = service.Provider.GetMailService()
	this.CacheService = service.Provider.GetCacheService()
}

func (this *AuthController) Register(c *gin.Context) {
	var registerReq reqModels.RegisterReq
	if err := c.ShouldBindJSON(&registerReq); err != nil {
		c.JSON(
			http.StatusBadRequest,
			dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)},
		)
		return
	}

	///Find a user in my sql database
	/// 1. If found -> return error existed
	user, err := this.AuthService.FindUserByPhoneNumber(registerReq.PhoneNumber)
	if user != nil {
		fmt.Println(err)
		c.JSON(http.StatusCreated, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.Created)})
		return
	}

	user, err = this.AuthService.FindUserByEmail(registerReq.Email)
	if user != nil {
		fmt.Println(err)
		c.JSON(http.StatusCreated, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.Created)})
		return
	}

	/// 2. crate new user and return token
	user, err = this.AuthService.CreateUser(registerReq)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	token, err := auth.CreateToken(user.UserID)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	c.JSON(
		http.StatusOK,
		dto.RegisterResponse{
			Meta: dto.NewMetaWithDefault(common.StatusOk),
			Data: dto.AccountInfo{
				AccountInfo: *user,
				Token:       token}})
	return
}

func (this *AuthController) Login(c *gin.Context) {
	/// get parameters
	var loginReq reqModels.LoginReq
	err := c.ShouldBindJSON(&loginReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"errors": err})
		return
	}

	var user *models.User = &models.User{Email: loginReq.Email, Password: loginReq.Password, PhoneNumber: loginReq.PhoneNumber}

	///validator login data
	err = user.Validate(models.LOGIN)
	if err != nil {
		logger.WorkoutLogger.Error(err)
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	if loginReq.PhoneNumber != "" {
		user, err = this.AuthService.FindUserByPhoneNumber(loginReq.PhoneNumber)
		if err != nil {
			c.JSON(404, gin.H{"code": 404, "error": err})
			return
		}
	}

	if loginReq.Email != "" {
		user, err = this.AuthService.FindUserByEmail(loginReq.Email)
		if err != nil {
			c.JSON(404, gin.H{"code": 404, "error": err})
			return
		}
	}

	err = models.VerifyPassword(user.Password, loginReq.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewMetaWithDefault(common.BadRequest))
		return
	}

	token, err := auth.CreateToken(user.UserID)
	if err != nil {
		c.JSON(404, gin.H{"code": 404, "error": err})
		return
	}

	data := dto.AccountInfo{AccountInfo: *user, Token: token}

	this.CacheService.MakeCreateOrReFreshToken(int64(user.UserID), token)
	resMsg := dto.LoginResponse{Meta: dto.NewMetaWithDefault(common.StatusOk), Data: data}
	c.JSON(http.StatusOK, resMsg)
	return
}

func (this *AuthController) Logout(c *gin.Context) {
	userId := getUserID(c)
	err := this.CacheService.MakeExpiredToken(userId)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewMetaWithDefault(common.BadRequest))
		return
	}

	c.JSON(http.StatusOK, dto.NewMetaWithDefault(common.StatusOk))
	return
}

func (this *AuthController) Update(c *gin.Context) {
	userId := getUserID(c)
	var updateReq reqModels.RegisterReq
	if err := c.ShouldBindJSON(&updateReq); err != nil {
		c.JSON(
			http.StatusBadRequest,
			dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)},
		)
		return
	}

	user, err := this.AuthService.FindUserByID(userId)
	if err != nil {
		c.JSON(http.StatusNotFound, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.NotFound)})
		return
	}

	user, err = this.AuthService.UpdateUser(user, updateReq)
	if err != nil {
		c.JSON(http.StatusConflict, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	c.JSON(http.StatusOK, dto.UpdateResponse{Meta: dto.NewMetaWithDefault(common.StatusOk), Data: *user})
	return
}

func (this *AuthController) ForgotPassword(c *gin.Context) {
	userId := getUserID(c)
	user, err := this.AuthService.FindUserByID(userId)

	if err != nil {
		c.JSON(http.StatusNotFound, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.NotFound)})
		return
	}

	userEmail := user.Email

	err = this.MailService.SendResetPassWordCode(userId, userEmail)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	c.JSON(http.StatusOK, dto.NewMetaWithDefault(common.StatusOk))
	return
}

func (this *AuthController) VerifyForgotPassWord(c *gin.Context) {
	userId := getUserID(c)
	verifyCode := c.Param("code")

	user, err := this.AuthService.FindUserByID(userId)
	if err != nil || user.ResetCode != verifyCode {
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	user.RefreshToken, err = auth.CreateToken(int32(userId))
	if err != nil {
		c.JSON(common.ServerInternal, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.ServerInternal)})
		return
	}

	go func() {
		this.AuthService.UpdateUserInfo(user)
	}()

	this.CacheService.MakeCreateOrReFreshToken(userId, user.RefreshToken)

	c.JSON(
		http.StatusOK,
		dto.VerifyResetPasswordResponse{
			Meta: dto.NewMetaWithDefault(common.StatusOk),
			Data: dto.AccountInfo{
				AccountInfo:  *user,
				RefreshToken: user.RefreshToken,
			},
		},
	)
	return
}

func (this *AuthController) ChangePassword(c *gin.Context) {
	var changePasswordReq reqModels.ChangePasswordReq
	err := c.ShouldBindJSON(&changePasswordReq)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	userId := getUserID(c)
	user, err := this.AuthService.FindUserByID(userId)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	user.Password = changePasswordReq.Password
	if err := user.Validate(models.UPDATE); err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseErrorResponse{Meta: dto.NewMetaWithDefault(common.BadRequest)})
		return
	}

	go func() {
		this.AuthService.UpdateUserInfo(user)
	}()

	c.JSON(
		http.StatusOK,
		dto.UpdateResponse{
			Meta: dto.NewMetaWithDefault(common.StatusOk),
			Data: *user,
		},
	)
}

func getUserID(c *gin.Context) int64 {
	return int64(c.Keys["user_id"].(int32))
}
