package controllers

import (
	"crawler-service/common"
	"crawler-service/dto"
	"crawler-service/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CrawlerController struct {
	BaseController
	CrawlerService      service.CrawlerService
	DriveCrawlerService service.DriveCrawlerService
}

var Crawler CrawlerController

func (this *CrawlerController) PrePare() {
	this.CrawlerService = service.Provider.GetCrawlerService()
	this.DriveCrawlerService = service.Provider.GetDriveCrawlerService()
}

func (this *CrawlerController) Crawl(c *gin.Context) {
	// _ = this.CrawlerService.Crawl()
	_ = this.DriveCrawlerService.Crawl()
	// if err != nil {
	// 	c.JSON(http.StatusBadRequest,
	// 		dto.BaseErrorResponse{
	// 			Meta: dto.NewMetaWithDefault(common.BadRequest),
	// 		},
	// 	)
	// 	return
	// }

	c.JSON(http.StatusOK, dto.NewMetaWithDefault(common.StatusOk))
	return
}
