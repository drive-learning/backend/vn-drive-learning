package command

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run API application",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		gin.Default().Run()
	},
}

func init() {
	rootCmd.AddCommand(runCmd)
}
