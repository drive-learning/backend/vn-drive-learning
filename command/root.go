package command

import (
	"fmt"
	"os"
	"crawler-service/logger"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "workout-auth-service",
	Short: "",
	Long:  "",
}

func Execute() {
	// rootCmd.Version = app.Version
	if err := rootCmd.Execute(); err != nil {
		logger.WorkoutLogger.Error(err)
		fmt.Print(err)
		os.Exit(1)
	}
}
