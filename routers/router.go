package routers

import (
	commonService "crawler-service/common"
	conf "crawler-service/configs"
	"crawler-service/controllers"
	"crawler-service/logger"
	middlewares "crawler-service/middleware"
	"crawler-service/models"
	"crawler-service/service"
	"fmt"

	session "github.com/ScottHuangZL/gin-jwt-session"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	orm "github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func init() {
	initConfig()
	initService()
	initDataBase()
	initRouters()
}

func HomeHandler(c *gin.Context) {
	respose := map[string]string{"message": "happy status"}
	c.JSON(200, respose)
}

func initRouters() {
	router := gin.Default()
	router.Use(session.ClearMiddleware())
	controllers.Auth.PrePare()
	controllers.Crawler.PrePare()

	v1 := router.Group("/v1")
	{
		v1.GET("/home", HomeHandler)
		v1.POST("/register", controllers.Auth.Register)
		v1.POST("/login", controllers.Auth.Login)
		v1.POST("/logout", middlewares.JWTAuthMiddleWare(), controllers.Auth.Logout)
		v1.POST("/update", middlewares.JWTAuthMiddleWare(), controllers.Auth.Update)
		v1.POST("/forgot-password", middlewares.JWTAuthMiddleWare(), controllers.Auth.ForgotPassword)
		v1.POST("/forgot-password/verify/:code", middlewares.JWTAuthMiddleWare(), controllers.Auth.VerifyForgotPassWord)
		v1.POST("/reset-password", middlewares.JWTAuthMiddleWare(), controllers.Auth.ChangePassword)

		v1.GET("/crawler/test", controllers.Crawler.Crawl)
	}

	router.Run()
}

func initConfig() {
	err := conf.InitConfig()
	if err != nil {
		logger.WorkoutLogger.Error("Can not setup configuration for Workout Authentication Service")
		panic(err)
	}
}

func initService() {

}

func initDataBase() {

	/// build mysql database datasource
	dataSource := fmt.Sprintf("%v:%v@(%v:%v)/%v?charset=utf8&parseTime=True&loc=Local",
		conf.ENVConfig.MySQLConfig.User,
		conf.ENVConfig.MySQLConfig.Password,
		conf.ENVConfig.MySQLConfig.Host,
		conf.ENVConfig.MySQLConfig.Port,
		conf.ENVConfig.MySQLConfig.Database)

	/// open database
	orm, err := orm.Open(commonService.DriverName, dataSource)
	if err != nil {
		logger.WorkoutLogger.Error(err)
		logger.WorkoutLogger.Error("Can not open database%v", dataSource)
	}

	/// create User Table
	if !orm.HasTable(&models.User{}) {
		orm.CreateTable(&models.User{})
	}

	// database, _ := sql.Open("sqlite3", "database/sql/learn_drive.db")
	// statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	// statement.Exec()

	database, err := gorm.Open(sqlite.Open("database/sql/learn_drive.sqlite"), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
	}

	database.AutoMigrate(&models.Category{})
	database.AutoMigrate(&models.Answer{})
	database.AutoMigrate(&models.SetQuestion{})
	database.AutoMigrate(&models.Question{})
	database.AutoMigrate(&models.HistoryLearning{})

	service.Provider = service.InitServiceProvider(orm, database)

	/// register models

}
