package service

import (
	"bytes"
	"html/template"
	"crawler-service/common"
	conf "crawler-service/configs"
	"crawler-service/dao"

	mail "gopkg.in/gomail.v2"
)

type MailService interface {
	SendResetPassWordCode(userId int64, toEmail string) error
	// GenerateCode() error
}

type MailServiceImpl struct {
	authDAO dao.AuthDAO
}

func NewMailService(authDAO dao.AuthDAO) MailService {
	return &MailServiceImpl{authDAO: authDAO}
}

func (this *MailServiceImpl) SendResetPassWordCode(userId int64, toEmail string) error {

	/// generate simple code has only digits
	generatedCode := common.GenerateForgotPasswordCode()

	/// update reset code of client
	go func() {
		this.authDAO.UpdateResetCode(userId, generatedCode)
	}()

	/// send email to client
	go func() {
		sendRequestReset("phamanhtuancross@gmail.com", generatedCode)
	}()

	return nil
}

func sendRequestReset(toEmail string, resetCode string) error {

	/// get mail body from template with resetcode
	msgBody := getResetPasswordMailContent(resetCode)

	/// configure sender email
	msg := mail.NewMessage()
	msg.SetAddressHeader("From", conf.ENVConfig.MailSmtpConfig.SenderAddress, conf.ENVConfig.MailSmtpConfig.SenderTitle)
	msg.SetHeader("To", toEmail)
	msg.SetHeader("Subject", conf.ENVConfig.MailSmtpConfig.SenderSubject)
	msg.SetBody("text/html", msgBody)

	m := mail.NewDialer(
		conf.ENVConfig.MailSmtpConfig.Host,
		conf.ENVConfig.MailSmtpConfig.Port,
		conf.ENVConfig.MailSmtpConfig.SenderAddress,
		conf.ENVConfig.MailSmtpConfig.SenderPassword)

	/// send email to user
	if err := m.DialAndSend(msg); err != nil {
		return err
	}

	return nil
}

func getResetPasswordMailContent(resetCode string) string {
	mailTemplate := template.Must(template.ParseFiles("./template/email.html"))
	var tpl bytes.Buffer
	mailTemplate.Execute(&tpl, template.FuncMap{"Code": resetCode})
	msgBody := tpl.String()
	return msgBody
}
