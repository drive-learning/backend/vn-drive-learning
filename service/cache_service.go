package service

import (
	"fmt"
	"strconv"

	"github.com/go-redis/redis"
)

type CacheService interface {
	MakeExpiredToken(userId int64) error
	MakeCreateOrReFreshToken(userId int64, newToken string) error
	GetValue(key string) (string, error)
}

type CacheServiceImpl struct {
	RedisClient *redis.Client
}

func NewCacheService(redisClient *redis.Client) CacheService {
	return &CacheServiceImpl{RedisClient: redisClient}
}

func (this *CacheServiceImpl) MakeExpiredToken(userId int64) error {
	key := strconv.Itoa(int(userId))
	err := this.RedisClient.Del(key).Err()
	return err
}

func (this *CacheServiceImpl) MakeCreateOrReFreshToken(userId int64, newToken string) error {
	key := strconv.Itoa(int(userId))
	err := this.RedisClient.Set(key, newToken, 0).Err()
	fmt.Println(err)
	return err

}

func (this *CacheServiceImpl) GetValue(key string) (string, error) {
	token, err := this.RedisClient.Get(key).Result()
	return token, err
}
