package service

import (
	"crawler-service/models"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

type CrawlerModeType = int16

const (
	Powerful CrawlerModeType = 1000
)

const (
	TimeSleepForLoadPageInSecond = 3 * time.Second
)

const (
	MangaDetailUrl      = "https://mangatoon.mobi/vi/detail/2026"
	MangaEpisodesUrl    = "https://mangatoon.mobi/vi/detail/2026/episodes"
	MangaHomeUrl        = "https://mangatoon.mobi"
	MangaMainCrawlerUrl = "https://mangatoon.mobi/vi/genre/comic"
)

const (
	MaxDepthForMangaDescription = 1
)

type CrawlerService interface {
	Crawl() error
	DriveCrawler() error
}

type CrawlerServiceImpl struct{}

func NewCrawlerService() CrawlerService {
	return &CrawlerServiceImpl{}
}

func (this *CrawlerServiceImpl) Crawl() error {
	// go this.crawManga(MangaDetailUrl, MangaEpisodesUrl)
	mainCollector := colly.NewCollector()

	mainCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").
			WithClassName("genre-top").
			WithTag("div").
			WithClassName("channels").
			Build(),
		func(mainElement *colly.HTMLElement) {
			mainElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("a").
					WithClassName("channel-a").
					Build(),
				func(_ int, chanelInfoElement *colly.HTMLElement) {
					chanelUrlInString := MangaHomeUrl + chanelInfoElement.Attr("href")
					go this.crawMangaInChanel(chanelUrlInString)
				})
		})

	mainCollector.OnRequest(func(req *colly.Request) {
		fmt.Println("Crawler is visiting Main crawler page", req.URL)
	})

	mainCollector.Visit(MangaMainCrawlerUrl)
	return nil
}

func (this *CrawlerServiceImpl) crawMangaInChanel(mangaChanelUrl string) error {
	mangaChanelCollector := colly.NewCollector()

	/// Get Manga's specific episode
	mangaChanelCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").
			WithClassName("genre-content").
			Build(),
		func(mangaTypeContentElement *colly.HTMLElement) {
			mangaTypeContentElement.ForEach("div.items a", func(_ int, mangaInfoElement *colly.HTMLElement) {
				mangaUrl := MangaHomeUrl + mangaInfoElement.Attr("href")
				mangaEpisodesUrl := mangaUrl + "/episodes"

				go this.crawManga(mangaUrl, mangaEpisodesUrl)
			})
		})

	/// Get Manga's next page

	mangaChanelCollector.OnRequest(func(req *colly.Request) {
		fmt.Println("crawler is visiting manga's type page with URL :", req.URL)
	})
	mangaChanelCollector.Visit(mangaChanelUrl)
	return nil
}

func (this *CrawlerServiceImpl) crawManga(mangaDetailUrl string, mangaEpisodesUrl string) error {

	go this.crawMangaDescription(mangaDetailUrl)
	go this.crawEpisodes(mangaEpisodesUrl)
	return nil
}

func (this *CrawlerServiceImpl) crawMangaDescription(mangaDetailUrl string) error {
	mangaDescriptionCollector := colly.NewCollector()
	var mangaId int

	mangaDescriptionCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").
			WithIdName("page-content").
			Build(),
		func(descriptionElement *colly.HTMLElement) {
			mangaBannerUrlInString := descriptionElement.ChildAttr(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassName("detail-top-wrap").
					WithTag("div").
					WithClassName("detail-top").
					WithTag("img").
					Build(), "src")

			mangaName := descriptionElement.ChildText(NewCSSSelectorBuilder().
				WithTag("div").
				WithClassName("detail-top-wrap").
				WithTag("h1").
				WithClassName("comics-title").
				Build())

			mangaAuthor := descriptionElement.ChildText(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassName("detail-top-wrap").
					WithTag("div").WithClassName("created-by").Build())

			mangaUpdatedAtInString := descriptionElement.ChildText(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassNames([]string{"selected-detail", "selected-tag"}).
					WithTag("div").
					WithClassName("icon-wrap").
					WithTag("span").
					WithClassName("update-date").
					Build())

			mangaDescription := descriptionElement.ChildText(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassNames([]string{"selected-detail", "selected-tag"}).
					WithTag("div").
					WithClassName("description").
					Build(),
			)

			mangaDescriptionTags := []string{}

			descriptionElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassNames([]string{"selected-detail", "selected-tag"}).
					WithTag("div").
					WithClassName("description-tag ").
					WithTag("div").
					WithClassName("tag").
					Build(),
				func(_ int, tagElement *colly.HTMLElement) {
					tag := tagElement.Text
					mangaDescriptionTags = append(mangaDescriptionTags, tag)
				})

			mangaDetailInfo := models.NewMangaInfo(
				mangaId,
				mangaName,
				mangaUpdatedAtInString,
				mangaAuthor,
				"",
				"",
				mangaDescription,
				mangaBannerUrlInString,
				mangaDescriptionTags,
			)

			fmt.Println(mangaDetailInfo)
		})

	mangaDescriptionCollector.OnRequest(func(req *colly.Request) {
		partsInRequestUrl := strings.Split(req.URL.Path, "/")
		if totalParts := len(partsInRequestUrl); totalParts > 0 {
			mangaId, _ = strconv.Atoi(partsInRequestUrl[totalParts-1])
		}

	})

	mangaDescriptionCollector.OnRequest(func(req *colly.Request) {
		fmt.Println("Crawler is visiting manga detail with url ", req.URL)
	})

	mangaDescriptionCollector.Visit(mangaDetailUrl)
	return nil
}

func (this *CrawlerServiceImpl) crawEpisodes(mangaEpisodesUrl string) error {

	parentMangaId := -1

	go func() {
		mangaEpisodesCollector := colly.NewCollector()

		mangaEpisodesCollector.OnHTML(
			NewCSSSelectorBuilder().
				WithTag("div").
				WithClassNames([]string{"selected-episodes", "selected-tag"}).
				Build(),
			func(episodesInfoElement *colly.HTMLElement) {
				episodesInfoElement.ForEach("div.episodes-wrap a.episode-item", func(_ int, element *colly.HTMLElement) {
					episodeUrlInString := MangaHomeUrl + element.Attr("href")
					this.crawEpisodeDetail(episodeUrlInString)
				})
			})

		mangaEpisodesCollector.OnRequest(func(req *colly.Request) {
			partsInRequestUrl := strings.Split(req.URL.Path, "/")
			if totalParts := len(partsInRequestUrl); totalParts > 1 {
				parentMangaId, _ = strconv.Atoi(partsInRequestUrl[totalParts-2])
				fmt.Println(parentMangaId)
			}
		})
		mangaEpisodesCollector.Visit(mangaEpisodesUrl)
	}()

	return nil
}

func (this *CrawlerServiceImpl) crawEpisodeDetail(mangaEpisodeDetailUrl string) error {
	episodeCollector := colly.NewCollector()
	episodeCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").
			WithIdName("page-content").
			WithTag("div").
			WithClassName("watch-page").
			Build(),
		func(episodeElement *colly.HTMLElement) {

			var pageNumber int = 0
			episodeElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("div").
					WithClassName("pictures").
					WithTag("img").
					Build(),
				func(_ int, pageElement *colly.HTMLElement) {
					pageNumber++
					contentUrlInString := pageElement.Attr("src")

					episodePage := models.NewEpisodePage(pageNumber, contentUrlInString)
					fmt.Println(episodePage)

				})

		})

	episodeCollector.OnRequest(func(req *colly.Request) {
		fmt.Println("Crawler is visiting episode with URL :", req.URL)
	})

	episodeCollector.Visit(mangaEpisodeDetailUrl)

	return nil
}

type CSSSelectorBuilder interface {
	WithTag(tag string) CSSSelectorBuilder
	WithIdName(idName string) CSSSelectorBuilder
	WithClassName(className string) CSSSelectorBuilder
	WithClassNames(classNames []string) CSSSelectorBuilder
	GetAll() CSSSelectorBuilder
	GetLastChild() CSSSelectorBuilder
	GetFirstChild() CSSSelectorBuilder
	WithEmpty() CSSSelectorBuilder
	Build() string
}

type CSSSelectorBuilderImpl struct {
	selectorContent string
}

func NewCSSSelectorBuilder() CSSSelectorBuilder {
	return &CSSSelectorBuilderImpl{selectorContent: ""}
}

func (this *CSSSelectorBuilderImpl) WithTag(tag string) CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s %s", this.selectorContent, tag)
	return this
}

func (this *CSSSelectorBuilderImpl) WithIdName(idName string) CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s[id=%s]", this.selectorContent, idName)
	return this
}

func (this *CSSSelectorBuilderImpl) WithClassName(className string) CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s[class=%s]", this.selectorContent, className)
	return this
}

func (this *CSSSelectorBuilderImpl) WithClassNames(classNames []string) CSSSelectorBuilder {
	for _, className := range classNames {
		this.selectorContent = fmt.Sprintf("%s.%s", this.selectorContent, className)
	}
	return this
}

func (this *CSSSelectorBuilderImpl) GetAll() CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s *", this.selectorContent)
	return this
}

func (this *CSSSelectorBuilderImpl) GetLastChild() CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s:last-child", this.selectorContent)
	return this
}

func (this *CSSSelectorBuilderImpl) GetFirstChild() CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s:first-child", this.selectorContent)
	return this
}

func (this *CSSSelectorBuilderImpl) WithEmpty() CSSSelectorBuilder {
	this.selectorContent = fmt.Sprintf("%s:empty", this.selectorContent)
	return this
}

func (this *CSSSelectorBuilderImpl) Build() string { return this.selectorContent }

/// MARK: - Driver page crawler
func (this *CrawlerServiceImpl) DriveCrawler() error {
	return nil
}
