package service

import (
	"crawler-service/configs"
	conf "crawler-service/configs"
	"crawler-service/dao"
	"fmt"

	"github.com/go-redis/redis"
	orm "github.com/jinzhu/gorm"
	"gorm.io/gorm"
)

type ServiceProvider interface {
	GetAuthService() AuthService
	GetMailService() MailService
	GetCacheService() CacheService
	GetCrawlerService() CrawlerService
	GetDriveCrawlerService() DriveCrawlerService
}

var Provider ServiceProvider

type ServiceProviderImpl struct {
	redisClient         *redis.Client
	ormHandler          *orm.DB
	gormDatabase        *gorm.DB
	authService         AuthService
	mailService         MailService
	cacheService        CacheService
	crawlerService      CrawlerService
	driveCrawlerService DriveCrawlerService
}

func InitServiceProvider(ormHandler *orm.DB, gormDatabase *gorm.DB) ServiceProvider {

	redisClient := initRedisClient()
	authDAO := dao.NewAuthDAO(ormHandler)
	driveLearningDAO := dao.NewDriveLearningDAO(gormDatabase)

	authService := NewAuthService(authDAO)
	mailService := NewMailService(authDAO)
	cacheService := NewCacheService(redisClient)
	crawlerService := NewCrawlerService()
	driveCrawlerService := NewDriveCrawlerService(driveLearningDAO)

	return &ServiceProviderImpl{
		ormHandler:          ormHandler,
		redisClient:         redisClient,
		authService:         authService,
		mailService:         mailService,
		cacheService:        cacheService,
		crawlerService:      crawlerService,
		driveCrawlerService: driveCrawlerService,
	}
}

func (this *ServiceProviderImpl) GetAuthService() AuthService {
	return this.authService
}

func (this *ServiceProviderImpl) GetCacheService() CacheService {
	return this.cacheService
}

func (this *ServiceProviderImpl) GetMailService() MailService {
	return this.mailService
}

func initRedisClient() *redis.Client {
	redisAddress := fmt.Sprintf("%v:%v", conf.ENVConfig.Redis.Host, conf.ENVConfig.Redis.Port)
	redisClient := redis.NewClient(&redis.Options{
		Addr:     redisAddress,
		DB:       int(conf.ENVConfig.Redis.DB),
		Password: configs.ENVConfig.Redis.Password,
	})

	return redisClient
	pong := redisClient.Ping()
	_, err := pong.Result()
	if err != nil {
		panic(err)
	}

	return redisClient
}

func (this *ServiceProviderImpl) GetCrawlerService() CrawlerService {
	return this.crawlerService
}

func (this *ServiceProviderImpl) GetDriveCrawlerService() DriveCrawlerService {
	return this.driveCrawlerService
}
