package service

import (
	"crawler-service/dao"
	"crawler-service/models"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/gocolly/colly"
	_ "github.com/mattn/go-sqlite3"
)

const (
	DrivePageForCrawlUrl        = "http://hoclaixeoto.vn/de-thi-thu-bang-lai-xe-may-a1-online/"
	RootImageDrivePageCrawlUrl  = "http://hoclaixeoto.vn/image/"
	ImageDatabasePrefix         = "database/images/"
	LearningImageDatabasePrefix = "database/images_learning/"

	SqlLiteDatabasePath = "database/sql/learn_drive.sqlite"
)

type DriveCrawlerService interface {
	Crawl() error
}

type DriveCrawlerServiceImpl struct {
	driveLearningDAO dao.DriveLearningDAO
}

func NewDriveCrawlerService(driveLearningDAO dao.DriveLearningDAO) DriveCrawlerService {
	result := &DriveCrawlerServiceImpl{driveLearningDAO: driveLearningDAO}
	return result
}

func (this *DriveCrawlerServiceImpl) Crawl() error {
	// mainCollector := colly.NewCollector()

	// mainCollector.OnHTML(
	// 	NewCSSSelectorBuilder().
	// 		WithTag("div").
	// 		WithClassName("container").
	// 		WithTag("div").
	// 		WithClassNames([]string{"d-none", "d-md-block"}).
	// 		Build(),
	// 	func(mainElement *colly.HTMLElement) {

	// 		mainElement.ForEach(
	// 			NewCSSSelectorBuilder().
	// 				WithTag("ul").WithClassNames([]string{"nav", "nav-pills"}).
	// 				WithTag("li:not(:first-child)").
	// 				Build(),
	// 			func(_ int, categoryElement *colly.HTMLElement) {
	// 				categoryUrl := categoryElement.ChildAttr(
	// 					NewCSSSelectorBuilder().
	// 						WithTag("a").
	// 						Build(),
	// 					"href")
	// 				go this.crawlAllSetOfQuestion(categoryUrl)
	// 			})
	// 	})
	// mainCollector.OnRequest(func(req *colly.Request) {
	// 	fmt.Println("Crawler is visiting Main crawler page", req.URL)
	// })

	// mainCollector.Visit(DrivePageForCrawlUrl)

	this.crawlAllSetOfQuestion("http://hoclaixeoto.vn/de-thi-thu-bang-lai-xe-may-a1-online/", "2")
	this.crawlAllSetOfQuestion("http://hoclaixeoto.vn/thi-bang-lai-xe-a2-online/", "1")
	this.crawlAllSetOfQuestion("http://hoclaixeoto.vn/thi-bang-lai-xe-b2-online/", "3")
	this.crawl200QuestionA1()
	return nil
}

func (this *DriveCrawlerServiceImpl) crawlAllSetOfQuestion(categoryUrl string, index string) error {

	mainCollector := colly.NewCollector()

	mainCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").WithClassName("container").
			WithTag("form").WithIdName("form2").
			Build(), func(mainElement *colly.HTMLElement) {

			descriptionIndex := 0
			categoryTitle := ""
			// categoryRule := ""
			categoryRuleDetail := ""

			mainElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("div").WithClassNames([]string{"col", "border", "border-primary", "rounded-lg", "p-2"}).
					WithTag("div:first-child").
					WithTag("p").
					Build(), func(_ int, categoryDescirpionElement *colly.HTMLElement) {
					content := categoryDescirpionElement.ChildText("strong")
					if descriptionIndex == 0 {
						categoryTitle = content
					}

					if descriptionIndex == 1 {
						// categoryRule = content
					}

					descriptionIndex++

				})

			mainElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("div").WithClassNames([]string{"col", "border", "border-primary", "rounded-lg", "p-2"}).
					WithTag("div:first-child").
					WithTag("li").Build(),
				func(_ int, ruleDetailElement *colly.HTMLElement) {
					categoryRuleDetail += ruleDetailElement.Text + "\n"
				})

			category := models.Category{Description: categoryRuleDetail, Title: categoryTitle, UpdateStatus: "Đang cập nhật...", IsLearningType: false, IsTestingType: true}
			this.driveLearningDAO.CreateCategory(&category)

			mainElement.ForEach(
				NewCSSSelectorBuilder().
					WithTag("button").
					WithClassNames([]string{"btn", "btn-success", "btn-thongtin"}).
					Build(), func(_ int, setValueElement *colly.HTMLElement) {

					setQuestionValue := setValueElement.Attr("value")
					go this.crawSetOfQuestion(categoryUrl, setQuestionValue, category.ID, index)
				})
		})
	mainCollector.OnRequest(func(req *colly.Request) {
		fmt.Println("Crawler is visiting Main crawler page", req.URL)
	})
	mainCollector.Visit(categoryUrl)
	return nil

}

func (this *DriveCrawlerServiceImpl) crawSetOfQuestion(categoryUrl string, setQuestionValue string, categoryId uint, index string) error {

	setQuestion := models.SetQuestion{CategoryId: categoryId}
	this.driveLearningDAO.CreateSetQuestion(&setQuestion)

	mainCollector := colly.NewCollector()

	mainCollector.OnHTML(NewCSSSelectorBuilder().
		WithTag("div").WithClassName("container").
		WithTag("form").WithIdName("form2").
		WithTag("div").WithClassNames([]string{"row", "p-2"}).
		Build(),
		func(element *colly.HTMLElement) {

			element.ForEach(
				NewCSSSelectorBuilder().
					WithTag("div").WithClassNames([]string{"row", "m-1", "content", "ndcauhoi"}).
					Build(),
				func(_ int, questionElement *colly.HTMLElement) {

					/// crawl question
					questionDescription := ""
					questionImageUrl := ""

					isFirstDiv := true
					questionElement.ForEach(
						NewCSSSelectorBuilder().
							WithTag("div").WithClassName("col").
							WithTag("div").
							WithTag("strong").Build(),
						func(_ int, questionDescriptionElement *colly.HTMLElement) {
							if isFirstDiv {
								isFirstDiv = false
							} else {
								questionDescription = questionDescriptionElement.Text
							}
						})

					questionImageUrl = questionElement.ChildAttr(
						NewCSSSelectorBuilder().
							WithTag("div").WithClassName("col").
							WithTag("img").WithClassNames([]string{"img-fluid", "hinhnho"}).
							Build(),
						"src")

					imageName := this.getImageName(questionImageUrl)
					question := models.Question{Description: questionDescription, ImageName: imageName, IsHasImage: imageName != "", SetQuestionId: setQuestion.ID}
					this.driveLearningDAO.CreateQuestion(&question)

					// answers := []models.Answer{}
					/// craw answers
					questionElement.ForEach(
						NewCSSSelectorBuilder().WithTag("div").WithClassNames([]string{"custom-control", "custom-radio", "cautraloi"}).
							Build(),
						func(_ int, answerElement *colly.HTMLElement) {
							content := answerElement.ChildText(NewCSSSelectorBuilder().WithTag("label").Build())
							isCorrect := strings.Contains(answerElement.ChildAttr(NewCSSSelectorBuilder().WithTag("label").Build(), "class"), "mauxanh")

							answer := models.Answer{Content: content, IsCorrect: isCorrect, QuestionId: question.ID}
							this.driveLearningDAO.CreateAnswer(&answer)

						})

				})

		})

	mainCollector.OnRequest(func(r *colly.Request) {

	})
	mainCollector.PostMultipart(categoryUrl, this.generateFormData(setQuestionValue, index))
	mainCollector.Wait()
	return nil
}

func (this *DriveCrawlerServiceImpl) generateFormData(setQuestionValue string, index string) map[string][]byte {
	return map[string][]byte{
		"chondethi": []byte(setQuestionValue),
		"chonde":    []byte(setQuestionValue + "-" + index),
		"nopbai":    []byte("nộp bài")}
}

func (this *DriveCrawlerServiceImpl) getImageName(suffixImagUrl string) string {
	splitElements := strings.Split(suffixImagUrl, "/")
	imageName := splitElements[len(splitElements)-1]
	if imageName == "0.jpg" {
		return ""
	}

	return imageName
}
func (this *DriveCrawlerServiceImpl) downloadQuestionImage(imageName string, imageURL string, pathPrefix string) error {
	if imageName == "" {
		return nil
	}

	/// make suffix to standard image's url
	_imageUrl := RootImageDrivePageCrawlUrl + imageName
	if imageURL != "" {
		_imageUrl = imageURL
	}
	file := this.createFile(imageName, pathPrefix)

	this.putFile(file, this.httpClient(), _imageUrl)

	fmt.Println(_imageUrl)
	return nil
}

func (this *DriveCrawlerServiceImpl) putFile(file *os.File, client *http.Client, imageUrl string) {
	resp, err := client.Get(imageUrl)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	size, err := io.Copy(file, resp.Body)

	defer file.Close()

	if err != nil {
		panic(err)
	}

	fmt.Println("Just Downloaded a file %s with size %d", imageUrl, size)
}

func (this *DriveCrawlerServiceImpl) httpClient() *http.Client {
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	return &client
}

func (this *DriveCrawlerServiceImpl) createFile(fileName string, pathPrefix string) *os.File {
	filePath := pathPrefix + fileName
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}

	return file
}

func (this *DriveCrawlerServiceImpl) crawl200QuestionA1() error {

	listCategoriesUrl := []string{"https://thibanglaixe.com.vn/200-cau-hoi-ly-thuyet-thi-sat-hach-bang-lai-xe-may-a1/",
		"https://thibanglaixe.com.vn/200-cau-hoi-ly-thuyet-thi-sat-hach-bang-lai-xe-may-a1/2/",
		"https://thibanglaixe.com.vn/200-cau-hoi-ly-thuyet-thi-sat-hach-bang-lai-xe-may-a1/3/"}

	mainCategory := models.Category{Title: "200 câu hỏi A1", IsTestingType: false, IsLearningType: true}
	this.driveLearningDAO.CreateCategory(&mainCategory)
	for index, value := range listCategoriesUrl {
		go this.crawl200QuestionA1ByPartURL(value, index, mainCategory.ID)
	}
	// for
	return nil
}

func (this *DriveCrawlerServiceImpl) crawl200QuestionA1ByPartURL(partURl string, partIndex int, categoryId uint) error {
	mainCollector := colly.NewCollector()

	setQuestionTitle := ""
	if partIndex == 0 {
		setQuestionTitle = "Lý thuyết"
	} else if partIndex == 1 {
		setQuestionTitle = "Biển báo"
	} else {
		setQuestionTitle = "Sa huỳnh"
	}

	setQuestion := models.SetQuestion{CategoryId: categoryId, LearningProgress: 0, Title: setQuestionTitle}
	this.driveLearningDAO.CreateSetQuestion(&setQuestion)

	mainCollector.OnHTML(
		NewCSSSelectorBuilder().
			WithTag("div").WithIdName("ftwp-postcontent").
			WithTag("div").WithClassName("to-mau-tai-lieu").
			Build(),
		func(mainElement *colly.HTMLElement) {

			listQuestions := []models.Question{}
			listAnswers := [][]models.Answer{}
			mainElement.ForEach(NewCSSSelectorBuilder().WithTag("p").Build(), func(_ int, questionElement *colly.HTMLElement) {

				questionDescription := questionElement.Text

				if len(questionDescription) > 10 {
					question := models.Question{Description: questionDescription}
					listQuestions = append(listQuestions, question)
				}

			})

			// currentQuestionIndex := 0
			mainElement.ForEach(NewCSSSelectorBuilder().WithTag("ol,ul").Build(), func(_ int, answersElement *colly.HTMLElement) {

				answers := []models.Answer{}
				answersElement.ForEach(NewCSSSelectorBuilder().WithTag("li").Build(), func(_ int, element *colly.HTMLElement) {
					content := element.Text
					isCorrect := element.ChildText(
						NewCSSSelectorBuilder().
							WithTag("li").
							WithTag("strong").
							Build()) != ""
					answer := models.Answer{Content: content, IsCorrect: isCorrect}

					answers = append(answers, answer)
				})
				listAnswers = append(listAnswers, answers)

				// currentQuestionIndex++
			})

			/// p [text] -> p[image] :
			///		- prev is empty
			///		- current isNotEmpty

			listImages := []string{}
			prevImage := ""
			mainElement.ForEach("p", func(_ int, element *colly.HTMLElement) {
				imageInStrong := element.ChildAttr(
					NewCSSSelectorBuilder().
						WithTag("strong").
						WithTag("img").
						Build(), "src")

				if imageInStrong != "" {
					// fmt.Println("Main strong ....", len(listImages), imageInStrong)
					listImages = append(listImages, imageInStrong)
					prevImage = imageInStrong
				} else {

					imageInB := element.ChildAttr(NewCSSSelectorBuilder().
						WithTag("b").
						WithTag("img").
						Build(), "src")

					if imageInB != "" {
						listImages = append(listImages, imageInStrong)
						prevImage = imageInStrong
					} else {

						if element.Text != "Giải thích: Biển 1 cấm mô tô, biển 2 cấm ô tô. Không phải cấm xe gắn máy." {
							imageURL := element.ChildAttr("img", "src")
							// fmt.Println("Image ....", len(listImages), imageInStrong)
							if len(listImages) == 0 {
								listImages = append(listImages, imageURL)
							} else {
								if imageURL != "" {
									if prevImage == "" {
										listImages[len(listImages)-1] = imageURL
									}
								} else {
									listImages = append(listImages, imageURL)
								}
							}
							prevImage = imageURL
						}
					}
				}
			})

			if partIndex == 1 {
				listQuestions = RemoveIndex(listQuestions, 1)
			}

			if partIndex == 2 {
				listImages = RemoveStringIndex(listImages, 1)

			}

			// fmt.Println("questoins length", len(listQuestions))
			// for index, image := range listImages {
			// 	fmt.Println("index at", index, image)
			// }

			// fmt.Println("question length", len(listQuestions))
			// fmt.Println("answers length", len(listAnswers))
			// fmt.Println("images length", len(listImages))
			for index := 0; index < len(listAnswers); index++ {
				question := listQuestions[index]
				if this.getImageName(listImages[index]) != "" {
					question.ImageName = "learning_" + this.getImageName(listImages[index])
				}
				question.IsHasImage = this.getImageName(listImages[index]) != ""
				question.SetQuestionId = setQuestion.ID
				go this.downloadQuestionImage(question.ImageName, listImages[index], LearningImageDatabasePrefix)

				this.driveLearningDAO.CreateQuestion(&question)

				answers := listAnswers[index]
				for _, answer := range answers {
					answer.QuestionId = question.ID
					this.driveLearningDAO.CreateAnswer(&answer)
				}
			}

			///downloaded images
			// for _, url := range listImages {
			// 	this.downloadQuestionImage()
			// }
			// for index := 0; index < 13; index++ {
			// 	fmt.Println("\n \nindex at ", index)
			// 	fmt.Print(listAnswers[index])
			// }

			// mainElement.ForEach(
			// 	NewCSSSelectorBuilder().
			// 		WithTag("p,img,ol").""
			// 		Build(),
			// 	func(_ int, element *colly.HTMLElement) {
			// 		fmt.Println("\n \n .....", element)
			// 	})

			// setQuestionTitle := mainElement.ChildText(
			// 	NewCSSSelectorBuilder().
			// 		WithTag("h2").WithClassNames([]string{"has-vivid-cyan-blue-color", "has-text-color"}).
			// 		WithTag("strong").
			// 		Build())
			// setQuestion := models.SetQuestion{CategoryId: mainCategory.ID, LearningProgress: 0.0, Title: setQuestionTitle}
			// fmt.Println(setQuestion)
			// this.driveLearningDAO.CreateSetQuestion(&setQuestion)

		})

	mainCollector.OnError(func(r *colly.Response, err error) {
		fmt.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	mainCollector.CheckHead = false

	mainCollector.OnResponse(func(res *colly.Response) {
		siteCookies := mainCollector.Cookies(res.Request.URL.RequestURI())
		println(siteCookies)
	})
	mainCollector.OnRequest(func(r *colly.Request) {

	})
	mainCollector.Visit(partURl)
	return nil
}

func RemoveIndex(questions []models.Question, index int) []models.Question {
	return append(questions[:index], questions[index+1:]...)
}

func RemoveStringIndex(strings []string, index int) []string {
	return append(strings[:index], strings[index+1:]...)
}
