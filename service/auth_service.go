package service

import (
	"crawler-service/dao"
	"crawler-service/models"
	"crawler-service/reqModels"
)

type AuthService interface {
	FindUserByID(userId int64) (*models.User, error)
	FindUserByPhoneNumber(phoneNumber string) (*models.User, error)
	FindUserByEmail(email string) (*models.User, error)
	CreateUser(userInfo reqModels.RegisterReq) (*models.User, error)
	UpdateUser(curUser *models.User, updateInfo reqModels.RegisterReq) (*models.User, error)
	UpdateUserInfo(user *models.User) (*models.User, error)
	UpdateResetCode(userId int64, newCode string) (*models.User, error)
}

type AuthServiceImpl struct {
	AuthDAO dao.AuthDAO
}

func NewAuthService(authDAO dao.AuthDAO) AuthService {
	return &AuthServiceImpl{AuthDAO: authDAO}
}

func (this *AuthServiceImpl) FindUserByID(userId int64) (*models.User, error) {
	return this.AuthDAO.FindByID(userId)
}
func (this *AuthServiceImpl) FindUserByPhoneNumber(phoneNumber string) (*models.User, error) {
	return this.AuthDAO.FindByPhoneNumber(phoneNumber)
}

func (this *AuthServiceImpl) FindUserByEmail(email string) (*models.User, error) {
	return this.AuthDAO.FindByEmail(email)
}

func (this *AuthServiceImpl) CreateUser(userInfo reqModels.RegisterReq) (*models.User, error) {
	return this.AuthDAO.Create(&models.User{
		Email:       userInfo.Email,
		PhoneNumber: userInfo.PhoneNumber,
		Password:    userInfo.Password,
		UserName:    userInfo.UserName})
}

func (this *AuthServiceImpl) UpdateUser(curUser *models.User, updateInfo reqModels.RegisterReq) (*models.User, error) {
	if updateInfo.Email != "" {
		curUser.Email = updateInfo.Email
	}

	if updateInfo.PhoneNumber != "" {
		curUser.PhoneNumber = updateInfo.PhoneNumber
	}

	if updateInfo.Password != "" {
		curUser.Password = updateInfo.Password
	}

	if updateInfo.UserName != "" {
		curUser.UserName = updateInfo.UserName
	}

	return this.AuthDAO.Update(curUser)
}

func (this *AuthServiceImpl) UpdateUserInfo(user *models.User) (*models.User, error) {
	return this.AuthDAO.Update(user)
}

func (this *AuthServiceImpl) UpdateResetCode(userId int64, newCode string) (*models.User, error) {
	return this.AuthDAO.UpdateResetCode(userId, newCode)
}
