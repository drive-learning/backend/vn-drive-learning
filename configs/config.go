package configs

import (
	"fmt"
	"os"
	"strconv"
	"crawler-service/logger"

	"github.com/joho/godotenv"
)

const (
	EnvironmentLocal = "local"
	EnvironmentDev   = "dev"
	EnvironmentQC    = "qc"
	EnvironmentUAT   = "uat"
)

type RedisConfig struct {
	Host            string
	Port            int64
	DB              int64
	Password        string
	CacheExpireTime int64
}

type JWTConfig struct {
	PublishKey string
}

type GinConfig struct {
}

type MySQLConfig struct {
	Host        string
	Port        string
	User        string
	Password    string
	Database    string
	MaxIdleConn int64
	MaxOpenConn int64
}

type MailSmtpConfig struct {
	Host           string
	Port           int
	SenderAddress  string
	SenderPassword string
	SenderTitle    string
	SenderSubject  string
}

type Config struct {
	Environment string
	GinConfig   GinConfig
	JWT         JWTConfig

	Redis          RedisConfig
	MySQLConfig    MySQLConfig
	MailSmtpConfig MailSmtpConfig
}

var ENVConfig *Config

func InitConfig() error {

	ENVConfig = &Config{}
	os.Setenv("ENVIRONMENT", "local")

	///load data config for local environment
	ENVConfig.Environment = os.Getenv("ENVIRONMENT")
	fmt.Println("ENVIRONMENT-OS-1")
	fmt.Println(ENVConfig.Environment)
	if ENVConfig.Environment == EnvironmentLocal {
		err := godotenv.Load(".env.local")
		if err != nil {
			logger.WorkoutLogger.Error("Error when load local config, error : ", err)
			panic(err)
		}
	}

	/// setup mysql configs
	ENVConfig.MySQLConfig.Host = os.Getenv("MYSQL_HOST")
	ENVConfig.MySQLConfig.Port = os.Getenv("MYSQL_PORT")
	ENVConfig.MySQLConfig.User = os.Getenv("MYSQL_USER")
	ENVConfig.MySQLConfig.Password = os.Getenv("MYSQL_PASSWORD")
	ENVConfig.MySQLConfig.Database = os.Getenv("MYSQL_DATABASE")

	/// setup mail smtp configs
	ENVConfig.MailSmtpConfig.Host = "smtp.gmail.com"
	ENVConfig.MailSmtpConfig.Port = 587
	ENVConfig.MailSmtpConfig.SenderAddress = os.Getenv("MAIL_SENDER_ADDRESS")
	ENVConfig.MailSmtpConfig.SenderPassword = os.Getenv("MAIL_SENDER_PASSWORD")
	ENVConfig.MailSmtpConfig.SenderTitle = os.Getenv("MAIL_SENDER_TITLE")
	ENVConfig.MailSmtpConfig.SenderSubject = os.Getenv("MAIL_SENDER_SUBJECT")

	/// setup redis config
	ENVConfig.Redis.Host = os.Getenv("REDIS_HOST")

	port, err := strconv.Atoi(os.Getenv("REDIS_PORT"))
	if err != nil {
		port = 6379
	}
	ENVConfig.Redis.Port = int64(port)
	ENVConfig.Redis.Password = os.Getenv("REDIS_PASSWORD")
	ENVConfig.Redis.CacheExpireTime = 60
	ENVConfig.Redis.DB = 0

	return nil
}
