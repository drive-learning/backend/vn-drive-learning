package middlewares

import (
	"crawler-service/auth"

	"github.com/gin-gonic/gin"
)

func JWTAuthMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId, err := auth.TokenValid(c.Request)
		if err != nil {
			respose := gin.H{"error": "UnAuthorized"}

			c.JSON(500, respose)
		}

		c.Set("user_id", userId)
		c.Next()
	}
}
