package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"crawler-service/logger"
	"crawler-service/service"

	"github.com/dgrijalva/jwt-go"
)

const (
	AuthorizedKey = "authorized"
	UserIDKey     = "user_id"
	ExpKey        = "exp"

	ExpiredTokenTimeInHour = 6

	TokenApiSecretKey      = "API_SECRET"
	TokenKey               = "token"
	HeaderAuthorizationKey = "Authorization"
)

func CreateToken(userId int32) (string, error) {

	privateKeyData, err := ioutil.ReadFile("id.rsa")
	if err != nil {
		return "", err
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyData)
	if err != nil {
		return "", err
	}

	token := jwt.New(jwt.SigningMethodRS256)
	claims := token.Claims.(jwt.MapClaims)
	claims[AuthorizedKey] = true
	claims[UserIDKey] = userId
	claims[ExpKey] = time.Now().Add(time.Hour * 24).Unix()
	return token.SignedString(privateKey)
}

func TokenValid(request *http.Request) (int32, error) {
	publicKey, err := ioutil.ReadFile("id.rsa.pub")
	if err != nil {
		return -1, fmt.Errorf("error reading public key file: %v\n", err)
	}

	key, err := jwt.ParseRSAPublicKeyFromPEM(publicKey)
	if err != nil {
		return -1, err
	}

	tokenInString := ExtractToken(request)
	token, err := jwt.Parse(tokenInString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return key, nil
	})

	if err != nil {
		return -1, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		Pretty(claims)
		userId := claims["user_id"].(float64)

		key := strconv.Itoa(int(userId))
		savedToken, err := service.Provider.GetCacheService().GetValue(key)
		if err != nil || savedToken != tokenInString {
			return -1, fmt.Errorf("Unexpected token")
		}

		return int32(userId), nil
	}

	return -1, fmt.Errorf("Unexpected claims")
}

func ExtractToken(request *http.Request) string {
	token := request.Header.Get(HeaderAuthorizationKey)

	if token != "" {
		return token
	}

	bearToken := request.Header.Get(HeaderAuthorizationKey)
	if len(strings.Split(bearToken, " ")) == 2 {
		return strings.Split(bearToken, " ")[1]
	}

	return ""
}

func Pretty(data interface{}) {
	b, err := json.MarshalIndent(data, "", "")
	if err != nil {
		logger.WorkoutLogger.Error(err)
	}

	logger.WorkoutLogger.Info(string(b))
}
