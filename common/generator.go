package common

import (
	"math/rand"
	"strconv"
	"time"
)

const MaxNumberDigits = 4

func GenerateForgotPasswordCode() string {
	var code string = ""
	for index := 0; index < MaxNumberDigits; index++ {
		rand.Seed(time.Now().UnixNano())
		code += strconv.Itoa(rand.Intn(10))
	}

	return code
}
