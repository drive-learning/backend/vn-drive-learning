package common

const (
	DriverName = "mysql"

	ErrorDescriptionUserNameEmpty   = "Required user name is not empty"
	ErrorDescriptionUserNameInValid = "Required user name valid format"

	ErrorDescriptionPasswordEmpty   = "Required password is not empty"
	ErrorDescriptionPasswordInValid = "Required password valid format"

	ErrorDescriptionEmailEmpty   = "Required email is not empty"
	ErrorDescriptionEmailInValid = "Required email valid format"

	ErrorDescriptionPhoneNumberEmpty   = "Required phone number is not empty"
	ErrorDescriptionPhoneNumberInValid = "Required phone number valid"

	StatusOk       = 200
	Created        = 201
	Accepted       = 202
	BadRequest     = 400
	Unauthorized   = 401
	Forbiden       = 402
	NotFound       = 404
	ServerInternal = 500

	StatusOKMsg       = "Status OK"
	CreatedMsg        = "Created"
	AcceptedMsg       = "Accepted"
	BadRequestMsg     = "Bad Request"
	UnauthorizedMsg   = "Unauthorized"
	ForbidenMsg       = "Forbiden"
	NotFoundMsg       = "Not Found"
	ServerInternalMsg = "Server Internal"
)
