package reqModels

type RegisterReq struct {
	UserName string `json:"user_name"`
	Email string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	Password string `json:"password"`
	Sex string `json:"sex"`
}