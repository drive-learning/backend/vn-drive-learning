package dto

type BaseErrorResponse struct {
	Meta Meta `json:"meta"`
}
