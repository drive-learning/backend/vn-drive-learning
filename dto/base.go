package dto

import "crawler-service/common"

type Meta struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func NewMetaWithDefault(code int) Meta {

	message := ""
	switch code {
	case common.StatusOk:
		message = common.StatusOKMsg
	case common.Accepted:
		message = common.AcceptedMsg
	case common.BadRequest:
		message = common.BadRequestMsg
	case common.Created:
		message = common.CreatedMsg
	case common.Unauthorized:
		message = common.UnauthorizedMsg
	case common.Forbiden:
		message = common.ForbidenMsg
	case common.NotFound:
		message = common.NotFoundMsg
	case common.ServerInternal:
		message = common.ServerInternalMsg
	default:
		break
	}

	return Meta{Code: code, Message: message}
}
