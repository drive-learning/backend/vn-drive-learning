package dto

import "crawler-service/models"

type AccountInfo struct {
	AccountInfo  models.User `json:"account_info"`
	Token        string      `json:"token"`
	RefreshToken string      `json:"refresh_token"`
}

type LoginResponse struct {
	Meta Meta        `json:"meta"`
	Data AccountInfo `json:"data"`
}

type RegisterResponse struct {
	Meta Meta        `json:"meta"`
	Data AccountInfo `json:"data"`
}

type UpdateResponse struct {
	Meta Meta        `json:"meta"`
	Data models.User `json:"data"`
}

type VerifyResetPasswordResponse struct {
	Meta Meta        `json:"meta"`
	Data AccountInfo `json:"data"`
}
