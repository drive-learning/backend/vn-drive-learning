package main

import (
	"crawler-service/command"
	_ "crawler-service/routers"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	command.Execute()
}
