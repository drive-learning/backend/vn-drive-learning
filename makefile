PROJECT ?= "workout-auth-service"
# GOPATH := $(PWD)
PROJECT_NAME := $(PROJect)
# PROJECT_PATH := "$(GOPATH)/$(PROJECT_NAME)"
# PATH := "$(PATH):$(GOPATH)/bin:/opt/golang/bin"
BUILD_DATE := `date +%FT%T%z`
VERSION_NAME := `cat $(PWD)/VERSION.md`


BUILD_FLAGS = -ldflags "-X $(PROJECT_NAME)/app.BuildDate=$(BUILD_DATE)\
						-X $(PROJECT_NAME)/app.BuildCommitHash=$(SHORT_SHA)\
						-X $(PROJECT_NAME)/app.VersionName=$(VERSION_NAME)\
						-X $(PROJECT_NAME)/app.VersionCode=$(BUILD_ID)"

.EXPORT_ALL_VARIABLES:
	ENV_LOCAL=local

hello:
	echo "Hello"

build:
	go build -o bin/main app.go

run local: 
	ENVIRONMENT=local go run app.go
